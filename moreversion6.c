#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>


#define	PAGELEN	20
#define	LINELEN	512


void do_more(FILE *);
int  get_input(FILE*);
double count=0.0;

int main(int argc , char *argv[])
{

   int i=0;
   if (argc == 1){
      do_more(stdin);
   }
   FILE * fp;
   FILE *fp1;
   while (++i < argc)
   {
      fp1 = fopen(argv[i],"r");
      char chr;
      if (fp1 == NULL)
      {
         perror("Can't open file");
         exit (1);
      } 
      else
      {
          chr=getc(fp1);
         while(chr !=EOF)
         {
            if(chr=='\n')
            count=count+1;
            chr=getc(fp1);
         }
      }
   }
   i=0;
   fclose(fp1);
   while(++i < argc)
   {
      fp = fopen(argv[i] , "r");
      if (fp == NULL)
      {
         perror("Can't open file");
         exit (1);
      } 
      do_more(fp);
      fclose(fp);
   }  
   return 0;
}

 
void do_more(FILE *fp)
{
   struct termios old_tio, new_tio;
   int res=0;
   res=tcgetattr(STDIN_FILENO, &old_tio);
   memcpy(&new_tio, &old_tio , sizeof(new_tio)
   new_tio.c_lflag &=(~ICANON );
   tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
   int num_of_lines = 0;
   int rv;
   char buffer[LINELEN];
   FILE* fp_tty = fopen("/dev//tty", "r");
   char ch;
   while (fgets(buffer, LINELEN, fp))
   {
      fputs(buffer, stdout);
      num_of_lines++;
      if (num_of_lines == PAGELEN){
         rv = get_input(fp_tty);
         if (rv == 0){
            printf("\033[1A \033[2K \033[1G");
            break;
         }
         else if (rv == 1){
            printf("\033[1A \033[2K \033[1G");
            num_of_lines -= PAGELEN;
         }
         else if (rv == 2){
            printf("\033[1A \033[2K \033[1G");
	         num_of_lines -= 1; 
            }
         else if (rv == 3){ 
            printf("\033[1A \033[2K \033[1G");
            break; 
         }
      }
  }
  res = tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
         
}
   float a=PAGELEN;
int get_input(FILE* cmdstream)
{
   int c;	
   
   float avg=(a/count)*100;
   printf("\033[7m --more--(%.0f%%) \033[m",avg);
   printf("\n");
     c = getc(cmdstream);
      if(c == 'q')
	 return 0;
      if ( c == ' ' )
      {
         a=a+PAGELEN;			
       	return 1;
      }
      if ( c == '\n' )	
      {
	     a=a+1;
        return 2;	
      }
      return 3;
   return 0;
}
