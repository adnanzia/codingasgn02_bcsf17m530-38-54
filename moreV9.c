#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

int	pl=30;
#define	LINELEN	512
extern char environ;


int z = 0;
void do_more(FILE *,char*);
int  get_input(FILE*, int*);
void PgSizeUpdate();
float count=0.0;
int pagesize=0;

int main(int argc , char *argv[])
{
   int i=0;
   if (argc == 1)
   {
      do_more(stdin,argv[i]);
   }
   FILE * fp;
   FILE *fp1;
   while (++i < argc)
   {
      fp1 = fopen(argv[i],"r");
      char chr;
      if (fp1 == NULL)
      {
         perror("Can't open file");
         exit (1);
      } 
      else
      {
          chr=getc(fp1);
         while(chr !=EOF)
         {
            if(chr=='\n')
            count=count+1;
            chr=getc(fp1);
         }
      }
   }
   i=0;
   fclose(fp1);
   while(++i < argc)
   {
      fp = fopen(argv[i] , "r");
      if (fp == NULL)
      {
         perror("Can't open file");
         exit (1);
      } 
      do_more(fp,argv[i]);
      fclose(fp);
   }  
   return 0;
}

 
void do_more(FILE *fp, char* fileName)
{

   

   int a=0;
   struct termios old_tio, new_tio;
   int rs=0;
   rs=tcgetattr(STDIN_FILENO, &old_tio);
   memcpy(&new_tio, &old_tio , sizeof(new_tio));
   new_tio.c_lflag &=(~ICANON );
   tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
   PgSizeUpdate();   
   int num_of_lines = 0;
   int rv;
   char buffer[LINELEN];
   FILE* fp_tty = fopen("/dev//tty", "r");
   char ch;
   char SK[100],P1[100],P2[100];
   while (fgets(buffer, LINELEN, fp))
   {
      fputs(buffer, stdout);
      num_of_lines++;
      a++;
      if (num_of_lines == pl)
      {
         rv = get_input(fp_tty,&a);
         if (rv == 0)
         {
            printf(" \033[2K \033[1G");
            break;//
         }
         else if (rv == 1)
         {
            printf(" \033[2K \033[1G");   
            num_of_lines -= pl;
         }
         else if (rv == 2)
         {//
            printf("\033[1A \033[2K \033[1G");
	         num_of_lines -= 1; 
         }

         else if(rv == 3)
         {
            printf(" \033[2K \033[1G");
            printf("/");
            fgets(SK,100,stdin); 
          

            int current_position = ftell(fp);
             int flag=0;
             int l = a;
            
             int temp = num_of_lines-1;   
            while(fgets(buffer,LINELEN,fp))
            {
               if(strcmp(buffer,SK)==0)
               {
                  printf("..../skipping\n");
                  printf("%s",P2 );
                  printf("%s",P1 );
                  printf("%s",SK );
                  flag=1;
                  break;
                 }
               strcpy(P2,P1);
               strcpy(P1,buffer);
               num_of_lines=4;
               a++;
            }
            
            if(flag==0){
                 	fseek(fp,current_position-1,SEEK_SET);
                 	num_of_lines = temp; 
                 		
                 	a=l;	z = 1;

                 }
         }
         else if (rv == 4)
         {
            printf("\033[1A \033[2K \033[1G");
            int pid=vfork();
            if(pid==0)
            {
               execlp("vim","vim",fileName,NULL);
               exit(0);
             }
            else
            {
               wait(0);
            }
         }
         else if (rv == 5)
         { 
            printf("\033[1A \033[2K \033[1G");
            break; 
         }
      }
  }
  rs = tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
}
int get_input(FILE* cmdstream,int *a)
{
   int c;	
   float avg=(*a/count)*100;
   PgSizeUpdate();
   if(z==1){
     printf("\033[7m --pattern not found\033[m");z=0;
   }
   else
   {
     printf("\033[7m --more--(%.0f%%) \033[m",avg);
   }
  
     c = getc(cmdstream);

      if(c == 'q' )
         return 0;
      if ( c == ' ' )		
       	return 1;
      if ( c == '\n' )	
        return 2;	
      if(c == '/')
         return 3;
      if (c == 'v' || c == 'V')
      {
         printf("\033[2K \033[1G"); 
         return 4;
      }
      
     return 5;
     return 0;
}
void PgSizeUpdate()
{
   struct winsize ws;
   ioctl(2,TIOCGWINSZ,&ws);
   pl=ws.ws_row-1;
}
